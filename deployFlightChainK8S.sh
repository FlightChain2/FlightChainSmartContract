#!/bin/bash



function getNPMPackageVersion {

    PACKAGE_VERSION=$(cat package.json \
      | grep version \
      | head -1 \
      | awk -F: '{ print $2 }' \
      | sed 's/[",]//g' \
      | tr -d '[[:space:]]')

    echo $PACKAGE_VERSION
}

export CHAINCODEVERSION=$(getNPMPackageVersion)
export CHANNEL="sitatranchannel"
export CHAINCODENAME="flightchain"
export PEER1=peer1st-sitaeu.fabric12:7051
export PEER2=peer2nd-sitaeu.fabric12:7051
export CORE_PEER_LOCALMSPID=sitaeuMSP
export ORDERER=orderer1st-sita.fabric12:7050
export ENDORSMENT_RULES="OR ('$CORE_PEER_LOCALMSPID.member')"

# set environment variables
export CORE_PEER_ID=peer1st-sitaus
export CORE_PEER_ADDRESS=peer1st-sitaus:7051
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/fabric12/orderers/orderer1st-sita.fabric12/msp/tlscacerts/tlsca.fabric12-cert.pem
export LANGUAGE="node"

# NODE_SRC_PATH *must* point to the directory in the docker container that contains the package.json and the chaincode
export NODE_SRC_PATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/FlightChainSmartContract


#
# Extract the command line arguments
#
POSITIONAL=()
while [[ $# -gt 0 ]]
do
	key="$1"
	case $key in
		-v | --version)
			CHAINCODEVERSION="$2"
			shift # past argument
			shift # past value
			;;

		-n | --chaincodename)
			CHAINCODENAME="$2"
			shift # past argument
			shift # past value
			;;

		-c | --channel)
			CHANNEL_NAME="$2"
			shift
			shift
			;;
		*)
		# unknown option
		 POSITIONAL+=("$1") # save it in an array for later
		 shift # past argument
		 ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters



echo ""
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"
echo "This script will deploy the chaincode onto the k8s network."
echo ""
echo "Default Values are: "
echo ""
echo "CHAINCODEVERSION=$CHAINCODEVERSION"
echo "CHANNEL=$CHANNEL"
echo "CHAINCODENAME=$CHAINCODENAME"
echo "PEER1=$PEER1"
echo "PEER2=$PEER2"
echo "CORE_PEER_LOCALMSPID=$CORE_PEER_LOCALMSPID"
echo "ORDERER=$ORDERER"
echo "ENDORSMENT_RULES=$ENDORSMENT_RULES"
echo "NODE_SRC_PATH=$NODE_SRC_PATH"
echo ""
echo "You can override these with the following command line parameters: ($0 -v <version> -n <chainCodeName> -c <channelName>)"
echo ""
echo "The command will run 'npm i', 'npm build', and then use the 'peer chaincode install/upgrade/instantiate' "
echo ""
echo ""



while true; do
    read -p "Is this first install, or an upgrade (ctrl-break to cancel)? (i/u)" yn
    case $yn in
        [Ii]* ) export UPGRADE=FALSE; break;;
        [Uu]* ) export UPGRADE=TRUE; break;;
        * ) echo "Please answer 'i' or 'u'.";;
    esac
done



if [ -d "node_modules" ]; then
    echo "Assuming node_modules is up to date, skipping 'npm install'"
    echo ""
else
    echo ""
    echo "Installing node_modules ..."
    echo ""
    npm install
fi



echo "Compiling & testing the chaincode typescript to javascript"

NPM_RTNVAL="$(npm run clean)"

NPM_RTNVAL="$(npm run build | grep error)"

if [[ $NPM_RTNVAL = *[!\ ]* ]]; then
  echo "Error with the npm run build command"
  echo ""
  echo "$NPM_RTNVAL"
  echo ""
  echo "Files in the dist folder..."
  ls -al dist
  echo ""
  echo ""
  while true; do
    read -p "There are errors in the build - do you want to continue? (y/n)" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer 'n' or 'y'.";;
    esac
  done
fi


echo "INSTALL ${CHAINCODENAME} ${CHAINCODEVERSION}"
set -x
export CORE_PEER_ADDRESS=$PEER1
peer chaincode install -n $CHAINCODENAME -v $CHAINCODEVERSION -p "$NODE_SRC_PATH" -l "$LANGUAGE" --tls --cafile=$ORDERER_CA
export CORE_PEER_ADDRESS=$PEER2
peer chaincode install -n $CHAINCODENAME -v $CHAINCODEVERSION -p "$NODE_SRC_PATH" -l "$LANGUAGE" --tls --cafile=$ORDERER_CA
set +x


if [ "$UPGRADE" = "TRUE" ]; then
    echo "PERFORMING AN UPGRADE OF ${CHAINCODENAME} ${CHAINCODEVERSION}, channel ${CHANNEL}"
    set -x
    export CORE_PEER_ADDRESS=$PEER1
    peer  chaincode upgrade -n $CHAINCODENAME -v $CHAINCODEVERSION -C $CHANNEL  -o $ORDERER -c '{"Args":[""]}' -P "${ENDORSMENT_RULES}" --tls --cafile=$ORDERER_CA
    export CORE_PEER_ADDRESS=$PEER2
    peer  chaincode upgrade -n $CHAINCODENAME -v $CHAINCODEVERSION -C $CHANNEL  -o $ORDERER -c '{"Args":[""]}' -P "${ENDORSMENT_RULES}" --tls --cafile=$ORDERER_CA
    set +x
else
    echo "PERFORMING AN INSTANTIATE OF ${CHAINCODENAME} ${CHAINCODEVERSION}, channel ${CHANNEL}"
    export CORE_PEER_ADDRESS=$PEER1
    peer chaincode instantiate -o $ORDERER -C $CHANNEL -n $CHAINCODENAME -v $CHAINCODEVERSION -l "$LANGUAGE" -c '{"Args":[""]}' -P "${ENDORSMENT_RULES}" --tls --cafile=$ORDERER_CA
    export CORE_PEER_ADDRESS=$PEER2
    peer chaincode instantiate -o $ORDERER -C $CHANNEL -n $CHAINCODENAME -v $CHAINCODEVERSION -l "$LANGUAGE" -c '{"Args":[""]}' -P "${ENDORSMENT_RULES}" --tls --cafile=$ORDERER_CA
fi


echo "INVOKE ${CHAINCODENAME} ${CHAINCODEVERSION}, channel ${CHANNEL}"
set -x
export CORE_PEER_ADDRESS=$PEER1
peer chaincode invoke -o $ORDERER -C $CHANNEL -n $CHAINCODENAME -c '{"function":"initLedger","Args":[""]}'
export CORE_PEER_ADDRESS=$PEER2
peer chaincode invoke -o $ORDERER -C $CHANNEL -n $CHAINCODENAME -c '{"function":"initLedger","Args":[""]}'
set +x
