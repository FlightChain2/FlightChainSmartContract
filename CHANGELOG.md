# Flight Chain ChainCode CHANGE LOG

## 2.0.5

- Fixed issue with missing fabric-shim dependency. 

## 2.0.4

- The createFlight method will now merge data if a flight already exists (previously it would return with an error saying flight already exists) 